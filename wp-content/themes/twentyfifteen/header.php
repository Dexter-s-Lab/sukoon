<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>


<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<?php wp_head() ?>
</head>

<div class="overlay" id="loading">
  <div class="spinner">
    <div class="rect1"></div>
    <div class="rect2"></div>
    <div class="rect3"></div>
    <div class="rect4"></div>
    <div class="rect5"></div>
  </div>
</div>

<div class="down_btn">
	<img src="assets/img/down_btn.png">
</div>

