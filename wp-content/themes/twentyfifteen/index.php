<?php

/*
 Template Name: Landing Page
 */

 ?>
<?php get_header(); ?>

<?php
global $post;

$p_id = get_the_ID();
$brochure = get_field('brochure',$p_id);

?>

<link href="style_home.css" rel="stylesheet" media="screen">


<title><?= $page_title; ?></title>

<div class="outer_container container" id="home">
  <?php  include 'nav_bar.php'; ?>
  <div class="slider_div">
    <?php
     echo do_shortcode('[rev_slider alias="home"]'); 
    ?>
  </div>
  <div class="sep_div" id="features"></div>
  <div class="feat_div scroll_divs">
    <div class="feat_row_1 container">
      <div class="loc_div col-sm-2 feat_cols feats_1" fID="1">
        <div class="icon_img">
          <img src="assets/img/features/loc_img.png">
          <span>location redefined</span>
        </div>
        <div class="feat_1_1 feats_sub">
          <p>
            Strategically located at the western side of the city on km 47 of Cairo-Alexandria Desert Road at Exit 3; far away from the congested Downtown of Cairo yet conveniently connected to it through many roads; The 26th of July Mehwar and soon linked by Mehwar Rod El Farag a 5- lane highway that takes you from Mohandiseen and Dokki. 
          </p>
          <p>
          The Location is 7 km away from the 
          entrance of Waslet Dahshour that 
          takes you to Juhayna Square and 
          Al Sheikh Zayed.
          </p>
          <p>
          Also located opposite to The West Cairo International Airport, and a few kilometers before Hyper One 
          Suleimaneyah, as well as El Fokka Road 
          that takes you directly to the heart 
          of the North-Coast.
           </p>
          <p>
          Our location is benefiting from the 
          Government-led investment in 
          transport links and infrastructure; 
          that will make it more connected 
          and accessible.
          </p>
        </div>
        <div class="feat_1_2 feats_sub">
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="town_div col-sm-2 feat_cols feats_2" fID="2">
        <div class="icon_img">
          <img src="assets/img/features/town_img.png">
          <span>townnhouses redefined</span>
        </div>
        <div class="feat_2 feats_sub">
          <p>
            At Sukoon™ we deeply believe that 
            townhouses are the best medium for 
            achieving and maintaining 
            the right balance. 
          </p>
          <p>
            So, we mindfully designed and 
            distributed 78 townhouses on total 
            build up area of 6000 m2 and 
            spacious total green area of 
            16,000 m2, to sum up total land 
            area of 22,000 m2.
          </p>
          <p>
            78 townhouses thoughtfully and 
            efficiently designed, beautifully and 
            generously distributed to accomplish the balance that Sukoon™ stands for.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="des_div col-sm-2 feat_cols feats_3" fID="3">
        <div class="icon_img">
          <img src="assets/img/features/des_img.png">
          <span>design & spaces redefined</span>
        </div>
        <div class="feat_3 feats_sub">
          <p>
            Spaces for us doesn't mean 
widths and heights, It means endless possibilities and countless
functionalities; where everything is designed to adequately utilize the maximum of spaces;  even the smallest of spaces are well thought of, lobbies, stairs, 
corners and hallways
          </p>
          <p>
            Mindfully designed Internally and 
externally to translate the Bauhaus™ aesthetics yet appeal to the 
contemporary and diverse
market needs.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="finish_div col-sm-2 feat_cols feats_4" fID="4">
        <div class="icon_img">
          <img src="assets/img/features/finish_img.png">
          <span>finishing redefined</span>
        </div>
        <div class="feat_4 feats_sub">
          <p>
            A proper standardized finishing guidelines enables us to deliver a distinctive end-to-end experience that maintains the balance we stand for.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="delivery_div col-sm-2 feat_cols feats_5" fID="5">
        <div class="icon_img">
          <img src="assets/img/features/delivery_img.png">
          <span>delivery date redefined</span>
        </div>
        <div class="feat_5 feats_sub">
          <p>
            A blend of sustainable, natural and 
cost efficient materials that 
collaboratively complement and enhance design, functionality and execution at all touch-points.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
    </div>
    <div class="feat_row_2 container">
      <div class="price_div col-sm-2 feat_cols feats_6" fID="6">
        <div class="icon_img">
          <img src="assets/img/features/price_img.png">
          <span>pricing redefined</span>
        </div>
        <div class="feat_6 feats_sub">
          <p>
            Reaching affordability governs 
everything we do, it starts with 
choosing the right land, minimizing 
construction, operational time and cost, standardizing finishings and unit sizes, through automation and better sourcing of efficient and sustainable building materials, while adopting the latest techniques to achieve the desired pricing. 
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="comm_div col-sm-2 feat_cols feats_7" fID="7">
        <div class="icon_img">
          <img src="assets/img/features/comm_img.png">
          <span>communities redefined</span>
        </div>
        <div class="feat_7 feats_sub">
          <p>
            Building a harmonious and 
balanced community is at the core of 
everything we do and everything we believe in, without it we can't get where 
we want to go.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="green_div col-sm-2 feat_cols feats_8" fID="8">
        <div class="icon_img">
          <img src="assets/img/features/green_img.png">
          <span>greenery redefined</span>
        </div>
        <div class="feat_8 feats_sub">
          <p>
            70 Percent private and shared total 
land area is devoted to greenery and 
fairly distributed over the 78 townhouses enabling our community to experience serenity and realize calmness.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="fac_div col-sm-2 feat_cols feats_9" fID="9">
        <div class="icon_img">
          <img src="assets/img/features/fac_img.png">
          <span>facilities redefined</span>
        </div>
        <div class="feat_9 feats_sub">
          <p>
            A highly secured and serene 
environment where its landscape and footpaths take you into a walk through swimming pools and fountains that center the communal gardens and are accessible and viewable from every window and terrace. 
</p>
<p>
At the gate, the club house is placed with a cozy lounge area and a mini gym along with a small convenience store that provides a variety of daily essentials 
directly next to a laundry service;
where a golf car can drive 
you back home.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="mat_div col-sm-2 feat_cols feats_10" fID="10">
        <div class="icon_img">
          <img src="assets/img/features/mat_img.png">
          <span>materials redefined</span>
        </div>
        <div class="feat_10 feats_sub">
          <p>
            As adequate as our spaces, 
we've efficiently planned and managed time, effectively controlled our deliverables thorough a set of practices and project management techniques to deliver in 30 months a properly finished community 
of townhouses ready for moving in.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
    </div>
    <div class="feat_row_1">
    </div>
  </div>
  <div class="feat_div_2 scroll_divs">
    <div class="container">
      <div class="loc_div col-sm-6 feat_cols feats_1 left_feat" fID="1">
        <div class="icon_img">
          <img src="assets/img/features/loc_img.png">
          <span>location redefined</span>
        </div>
        <div class="feat_1_1 feat_1 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/loc_img.png">
          <span>location redefined</span>
          </div>
          <p>
            Strategically located at the western side of the city on km 47 of Cairo-Alexandria Desert Road at Exit 3; far away from the congested Downtown of Cairo yet conveniently connected to it through many roads; The 26th of July Mehwar and soon linked by Mehwar Rod El Farag a 5- lane highway that takes you from Mohandiseen and Dokki. 
          </p>
          <p>
          The Location is 7 km away from the 
          entrance of Waslet Dahshour that 
          takes you to Juhayna Square and 
          Al Sheikh Zayed.
          </p>
          <p>
          Also located opposite to The West Cairo International Airport, and a few kilometers before Hyper One 
          Suleimaneyah, as well as El Fokka Road 
          that takes you directly to the heart 
          of the North-Coast.
           </p>
          <p>
          Our location is benefiting from the 
          Government-led investment in 
          transport links and infrastructure; 
          that will make it more connected 
          and accessible.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="town_div col-sm-6 feat_cols feats_2 right_feat" fID="2">
        <div class="icon_img">
          <img src="assets/img/features/town_img.png">
          <span>townnhouses redefined</span>
        </div>
        <div class="feat_2 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/town_img.png">
            <span>townnhouses redefined</span>
          </div>
          <p>
            At Sukoon™ we deeply believe that 
            townhouses are the best medium for 
            achieving and maintaining 
            the right balance. 
          </p>
          <p>
            So, we mindfully designed and 
            distributed 78 townhouses on total 
            build up area of 6000 m2 and 
            spacious total green area of 
            16,000 m2, to sum up total land 
            area of 22,000 m2.
          </p>
          <p>
            78 townhouses thoughtfully and 
            efficiently designed, beautifully and 
            generously distributed to accomplish the balance that Sukoon™ stands for.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
    </div>
    <div class="container">
      <div class="finish_div col-sm-6 feat_cols feats_4 left_feat" fID="4">
        <div class="icon_img">
          <img src="assets/img/features/finish_img.png">
          <span>finishing redefined</span>
        </div>
        <div class="feat_4 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/finish_img.png">
            <span>finishing redefined</span>
          </div>
          <p>
            A proper standardized finishing guidelines enables us to deliver a distinctive end-to-end experience that maintains the balance we stand for.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="des_div col-sm-6 feat_cols feats_3 right_feat" fID="3">
        <div class="icon_img">
          <img src="assets/img/features/des_img.png">
          <span>design & spaces redefined</span>
        </div>
        <div class="feat_3 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/des_img.png">
            <span>design & spaces redefined</span>
          </div>
          <p>
            Spaces for us doesn't mean 
widths and heights, It means endless possibilities and countless
functionalities; where everything is designed to adequately utilize the maximum of spaces;  even the smallest of spaces are well thought of, lobbies, stairs, 
corners and hallways
          </p>
          <p>
            Mindfully designed Internally and 
externally to translate the Bauhaus™ aesthetics yet appeal to the 
contemporary and diverse
market needs.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
    </div>
    <div class="container">
      <div class="delivery_div col-sm-6 feat_cols feats_5 left_feat" fID="5">
        <div class="icon_img">
          <img src="assets/img/features/delivery_img.png">
          <span>delivery date redefined</span>
        </div>
        <div class="feat_5 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/delivery_img.png">
            <span>delivery date redefined</span>
          </div>
          <p>
            A blend of sustainable, natural and 
cost efficient materials that 
collaboratively complement and enhance design, functionality and execution at all touch-points.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="price_div col-sm-6 feat_cols feats_6 right_feat" fID="6">
        <div class="icon_img">
          <img src="assets/img/features/price_img.png">
          <span>pricing redefined</span>
        </div>
        <div class="feat_6 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/price_img.png">
            <span>pricing redefined</span>
          </div>
          <p>
            Reaching affordability governs 
everything we do, it starts with 
choosing the right land, minimizing 
construction, operational time and cost, standardizing finishings and unit sizes, through automation and better sourcing of efficient and sustainable building materials, while adopting the latest techniques to achieve the desired pricing. 
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      </div>
      <div class="container">
      <div class="comm_div col-sm-6 feat_cols feats_7 left_feat" fID="7">
        <div class="icon_img">
          <img src="assets/img/features/comm_img.png">
          <span>communities redefined</span>
        </div>
        <div class="feat_7 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/comm_img.png">
            <span>communities redefined</span>
          </div>
          <p>
            Building a harmonious and 
balanced community is at the core of 
everything we do and everything we believe in, without it we can't get where 
we want to go.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="green_div col-sm-6 feat_cols feats_8 right_feat" fID="8">
        <div class="icon_img">
          <img src="assets/img/features/green_img.png">
          <span>greenery redefined</span>
        </div>
        <div class="feat_8 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/green_img.png">
            <span>greenery redefined</span>
          </div>
          <p>
            70 Percent private and shared total 
land area is devoted to greenery and 
fairly distributed over the 78 townhouses enabling our community to experience serenity and realize calmness.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      </div>
      <div class="container">
      <div class="fac_div col-sm-6 feat_cols feats_9 left_feat" fID="9">
        <div class="icon_img">
          <img src="assets/img/features/fac_img.png">
          <span>facilities redefined</span>
        </div>
        <div class="feat_9 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/fac_img.png">
            <span>facilities redefined</span>
          </div>
          <p>
            A highly secured and serene 
environment where its landscape and footpaths take you into a walk through swimming pools and fountains that center the communal gardens and are accessible and viewable from every window and terrace. 
</p>
<p>
At the gate, the club house is placed with a cozy lounge area and a mini gym along with a small convenience store that provides a variety of daily essentials 
directly next to a laundry service;
where a golf car can drive 
you back home.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
      <div class="mat_div col-sm-6 feat_cols feats_10 right_feat" fID="10">
        <div class="icon_img">
          <img src="assets/img/features/mat_img.png">
          <span>materials redefined</span>
        </div>
        <div class="feat_10 feats_sub">
          <div class="icon_img">
            <img src="assets/img/features/mat_img.png">
            <span>materials redefined</span>
          </div>
          <p>
            As adequate as our spaces, 
we've efficiently planned and managed time, effectively controlled our deliverables thorough a set of practices and project management techniques to deliver in 30 months a properly finished community 
of townhouses ready for moving in.
          </p>
        </div>
        <div class="custom_overlay"></div>
      </div>
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_1 scroll_divs" id="models">
    <div class="model_1_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">173 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">Town</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_img_div">
      <img src="assets/img/model_1_img_1.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_1_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">173 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">Town</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_img_div">
      <img src="assets/img/model_town_1_2.jpg">
    </div>
  </div>
  <div class="model_1_2">
    <div class="model_1_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">173 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">Town</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_2_img_div">
      <div class="model_1_title">
        <span>Ground Floor</span>
      </div>
      <img src="assets/img/model_1_img_2.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_1_2">
    <div class="model_1_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">173 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">Town</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_2_img_div">
      <div class="model_1_title">
        <span>First Floor</span>
      </div>
      <img src="assets/img/model_1_img_3.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_1_2">
    <div class="model_1_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">173 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">Town</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_2_img_div light_custom">
      <div class="model_1_title">
        <span>Pent House</span>
      </div>
      <img src="assets/img/model_town_1_5.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_2">
    <div class="model_2_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">223 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">TownPlus</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_img_div">
      <img src="assets/img/model_town_plus_1_1.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_2">
    <div class="model_2_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">223 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">TownPlus</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_img_div">
      <img src="assets/img/model_town_plus_1_2.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_2_2">
    <div class="model_2_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">223 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">TownPlus</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_2_img_div">
      <div class="model_2_title">
        <span>Ground Floor</span>
      </div>
      <img src="assets/img/model_2_img_2.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_2_2">
    <div class="model_2_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">223 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">TownPlus</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_2_img_div">
      <div class="model_2_title">
        <span>First Floor</span>
      </div>
      <img src="assets/img/model_2_img_3.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_2">
    <div class="model_3_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">223 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">TwinTown</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_img_div">
      <img src="assets/img/model_twin_town_1_1.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_1_2">
    <div class="model_3_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">223 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">TwinTown</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_2_img_div light_custom">
      <div class="model_3_title">
        <span>Ground Floor</span>
      </div>
      <img src="assets/img/model_twin_town_1_2.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="model_1_2">
    <div class="model_3_header">
      <div class="models_spans_div_left">
        <span class="bottom_span spans">223 SQM</span>
      </div>
      <div class="models_spans_div_right">
        <span class="top_span spans">model</span>
        <span class="bottom_span spans">TwinTown</span>
        <!-- <span>&#8482;</span> -->
      </div>
    </div>
    <div class="model_1_2_img_div light_custom">
      <div class="model_3_title">
        <span>First Floor</span>
      </div>
      <img src="assets/img/model_twin_town_1_3.jpg">
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="master_plan scroll_divs" id="master_plan">
    <div class="master_plan_header">
      <div class="master_plan_spans_div_left">
        <span class="top_span spans">Master</span>
        <span class="bottom_span spans">Plan</span>
      </div>
    </div>
    <div class="master_plan_img_div container">
      <div class="col-sm-3 master_plan_text">
        <p>
        78 townhouses thoughtfully and efficiently 
designed, beautifully and generously 
distributed where all elements work 
together harmoniously, to accomplish the balance that Sukoon™ stands for.
      </p>
      </div>
       <div class="col-sm-7">
      <img src="assets/img/master_plan.jpg">
      </div>
    </div>
  </div>
  <div class="sep_div"></div>
  <div class="direction_map scroll_divs" id="direction_map">
    <div class="master_plan_header">
      <div class="master_plan_spans_div_left">
        <span class="top_span spans">Direction</span>
        <span class="bottom_span spans">Map</span>
      </div>
    </div>
    <div class="direction_map_img_div">
      <img src="assets/img/direction_map.jpg">
    </div>
  </div>
  <div class="sep_div" id="the_developers"></div>
  <div class="about_dev scroll_divs">
    <div class="about_dev_header">
      <div class="about_dev_spans_div_left">
        <span class="top_span spans">About</span>
        <span class="bottom_span spans">The Developers</span>
      </div>
    </div>
    <div class="about_dev_cols container">
      <div class="about_left_col col-sm-6">
        <div class="col-sm-4">
        </div>
        <div class="about_left_col_mid col-sm-7">
          <div class="about_left_col_right">
            <img src="assets/img/about_left.png">
          </div>
          <p>
          Founded by Khaled Edrees in 2009,  with a wide range of investments and services centered on developing in the field of facility management, industrial real-estate, commercial real-estate and residential real-estate.
        </p>
<p>
Marafeq investments has a large portfolio of multinational,regional 
and national clients in different 
industries and domains.
</p>
<p>
This exposure gave Marafeq 
a wide experience, knowledge and credebility in planning, executing and delivering 
projects; efficiently and effectively.
        </p>
        </div>
        <div class="col-sm-1">
        </div>
      </div>
      <div class="about_right_col col-sm-6">
        <div class="col-sm-3">
        </div>

      <div class="about_right_col_1 col-sm-8">
        <div class="about_right_col_3">
          <img src="assets/img/about_right.png">
        </div>
        <p>
          Founded in 1946 by the architect 
Yehia Mostafa with a large and diversified portfolio of projects and clients that varies from private sector to public sector, 
national clients to regional clients from different fields.
</p>
<p>
The firm designed and supervised hundreds of governmental, residential, industrial, medical, touristic and educational buildings, campuses and resorts and was honored to 
participate in the first renovation and expansion of the Grand Mosque in Mekkah.
        </p>
        <p>
          The firm continued to grow and
expand its portfolio by the Architect Hesham Yehia Mostafa 
through designing and
supervising several projects 
such as Makadi Bay, Gouna by Travco,  multiple Phases in 
Wady El Nakhil, Rofayda Health park and over 300 separate villas all across Egypt.
        </p>
      </div>
      <div class="col-sm-1">
      </div>
      
    </div>
    </div>
  </div>

  <div class="about_dev_2 scroll_divs">
    <div class="about_dev_header">
      <div class="about_dev_spans_div_left">
        <span class="top_span spans">About</span>
        <span class="bottom_span spans">The Developers</span>
      </div>
    </div>
    <div class="about_dev_cols container">
      <div class="about_left_col col-sm-6">
        <div class="col-sm-3">
        </div>
        <div class="about_left_col_right col-sm-4">
          <img src="assets/img/about_left.png">
        </div>
        <div class="about_left_col_mid col-sm-5">
          <p>
          Founded by Khaled Edrees in 2009,  with a wide range of investments and services centered on developing in the field of facility management, industrial real-estate, commercial real-estate and residential real-estate.
        </p>
<p>
Marafeq investments has a large portfolio of multinational,regional 
and national clients in different 
industries and domains.
</p>
<p>
This exposure gave Marafeq 
a wide experience, knowledge and credebility in planning, executing and delivering 
projects; efficiently and effectively.
        </p>
        </div>
      </div>
      <div class="about_right_col col-sm-6">
        <div class="about_right_col_3 col-sm-4">
        <img src="assets/img/about_right.png">
      </div>
      <div class="about_right_col_2 col-sm-4">
        <p>
          Founded in 1946 by the architect 
Yehia Mostafa with a large and diversified portfolio of projects and clients that varies from private sector to public sector, 
national clients to regional clients from different fields.
</p>
<p>
The firm designed and supervised hundreds of governmental, residential, industrial, medical, touristic and educational buildings, campuses and resorts and was honored to 
participate in the first renovation and expansion of the Grand Mosque in Mekkah.
        </p>
        <p>
          The firm continued to grow and
expand its portfolio by the Architect Hesham Yehia Mostafa 
through designing and
supervising several projects 
such as Makadi Bay, Gouna by Travco,  multiple Phases in 
Wady El Nakhil, Rofayda Health park and over 300 separate villas all across Egypt.
        </p>
      </div>
    </div>
    </div>
  </div>


  <div class="sep_div"></div>
  <div class="footer scroll_divs" id="contacts">
      <div class="footer_spans_div_left">
        <span class="spans block_spans">1103 Korniche El Nil, Gareden City, Cairo</span>
        <span class="spans">Office: 02 27947344</span><span class="spans right_span">Sales: 0 111 111 1927</span>
        <span class="spans block_spans last_span">info@sukoon.life</span>
      </div>
      <div class="footer_spans_div_right">
        <span class="spans">A Project By </span>
        <img src="assets/img/about_left.png">
      </div>
    </div>
</div>

<?php include 'modal_includes.php'; ?>

<?php get_footer(); ?>

<script>

var active = 0;
var newHeight = 0;

$( document ).ready(function() {

  $('.bro_btn').on('click',function (e) {

    e.preventDefault();

    var flag1 = $('.mobile_nav').is(':visible');
    var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

    if(flag1 && flag2) 
    $('.navbar-toggle').click();

    $('.md_trigger_form').trigger('click');
    $('.md-close').fadeIn(1800);

  });

  $('.pdf_submit').on('submit',function (e) {

    
    e.preventDefault();
    return false;
    $('.send_spin').show();
    $('.pdf_submit_btn').hide();

    // var x = $(this).serialize();

    var formID = $(this).attr('formID');
    var inputs = $('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);

    });

    var formID = $(this).attr('formID'); 
    $('#gform_' + formID).submit();
    // console.log(x);
    // return false;
    
    jQuery(document).bind('gform_post_render', function(){
   
    $('.send_spin').hide();
    $('.succ_div').show();

    setTimeout(function(){

    $('.succ_div').hide();
    $('.download_pdf_btn').fadeIn(500);
    $('.f_1').fadeOut(500);
    $('.f_2').fadeOut(500);
    $('.md-content h3').hide();

   }, 

     1500);

    
  });

   });

  $('.mobile_links').on('click',function (e) {

      e.preventDefault();

      var target = this.hash;
      var $target = $(target);

      $('html, body').stop().animate({
          'scrollTop': $target.offset().top - newHeight
      }, 900, 'swing', function () {
          
      });

      var flag1 = $('.mobile_nav').is(':visible');
      var flag2 = $('.mobile_nav .navbar-collapse').is(':visible');

      if(flag1 && flag2) 
      $('.navbar-toggle').click();
  });

  $('.down_btn').on('click',function (e) {

    e.preventDefault();

    $('.scroll_divs').each(function() {
      var post = $(this);
      var position = post.position().top - $(window).scrollTop();


      if (position > newHeight) 
      {
          $('html, body').stop().animate({
                'scrollTop': post.offset().top - newHeight
            }, 900, 'swing', function () {
                
            });
          return false;
      }
  }); 

    });

  $('.nav_links').on('click',function (e) {

    e.preventDefault();

    var target = this.hash;
    var $target = $(target);
    var new_offset = parseInt($('.header_div').css('height'));
    $('html, body').stop().animate({
          'scrollTop': $target.offset().top - new_offset
      }, 900, 'swing', function () {
          
      });

    });

  function hideFeat()
  {
    if(active == 9)
    {
      $('.feat_div .feat_' + active).css('left', 0);
      $('.feat_div .feat_' + active).css('bottom', 0);
      $('.feat_div .feat_' + active).css('opacity', 0);
      $('.feat_div .feat_' + active).css('height', 0);
    }

    else if(active == 10)
    {
      $('.feat_div .feat_' + active).css('right', 0);
      $('.feat_div .feat_' + active).css('top', 0);
      $('.feat_div .feat_' + active).css('opacity', 0);
      $('.feat_div .feat_' + active).css('height', 0);
    } 

    else
    {
      $('.feat_div .feat_' + active).css('left', 0);
      $('.feat_div .feat_' + active).css('top', 0);
      $('.feat_div .feat_' + active).css('opacity', 0);
      $('.feat_div .feat_' + active).css('height', 0);
      $('.feat_div .feat_' + active + '_1').css('left', 0);
      $('.feat_div .feat_' + active + '_1').css('top', 0);
      $('.feat_div .feat_' + active + '_1').css('opacity', 0);
      $('.feat_div .feat_' + active + '_1').css('height', 0);
      $('.feat_div .feat_' + active + '_2').css('left', 0);
      $('.feat_div .feat_' + active + '_2').css('top', 0);
      $('.feat_div .feat_' + active + '_2').css('opacity', 0);
      $('.feat_div .feat_' + active + '_2').css('height', 0);
    } 
  }

  $('.feat_div_2 .feat_cols').on('click',function (e) {

    e.preventDefault();

    var fID = parseInt($(this).attr('fID'));

    if($(this).find('.feats_sub').css('opacity') == 1)
    {
      
      $(this).find('.feats_sub').css('height', 0);
      $(this).find('.feats_sub').css('width', '100%');
      $(this).find('.feats_sub').css('opacity', 0);

      $('.custom_overlay').hide();
    } 

    else
    {

      $('.feat_'+active).css('height', 0);
      $('.feat_'+active).css('width', '100%');
      $('.feat_'+active).css('opacity', 0);

      var window_height = $(window).height();
      var head_height2 = parseInt($('.header_div').css('height'));
      window_height = window_height - head_height2;
      // window_height = window_height - head_height2 + 10;

      $(this).find('.feats_sub').css('height', window_height);
      $(this).find('.feats_sub').css('width', '200%');
      $(this).find('.feats_sub').css('opacity', 1);


      active = fID;

      $('.feat_cols .custom_overlay').show();
      $(this).find('.custom_overlay').hide();

      $('html, body').animate({
        // scrollTop: $(this).offset().top - head_height2 - 10
        scrollTop: $(this).offset().top - head_height2
    }, 'slow');
    } 

    });

  $('.feat_div .feats_1').on('click',function (e) {

    e.preventDefault();
    if($('.feat_div .feat_1_1').css('opacity') == 1)
    {
      
      var h1 = parseInt($(this).css('height'));
      h1 = h1 + 'px';

      $('.feat_div .feat_1_1').css('height', h1);
      $('.feat_div .feat_1_1').css('left', 0);
      $('.feat_div .feat_1_1').css('opacity', 0);

      $('.feat_div .feat_1_2').css('height', h1);
      $('.feat_div .feat_1_2').css('top', 0);
      $('.feat_div .feat_1_2').css('opacity', 0);

      $('.feat_div .custom_overlay').hide();
    } 

    else
    {
      hideFeat();

      var height = parseInt($(this).css('height'));
      var width = parseInt($(this).css('width'));
      width = width + 'px';
      
      var h1 = height * 2;
      var h2 = height;

      h1 = h1 + 'px';
      h2 = h2 + 'px';


      $('.feat_1_1').css('height', h1);
      $('.feat_1_1').css('left', width);
      $('.feat_1_1').css('opacity', 1);

      $('.feat_1_2').css('height', h2);
      $('.feat_1_2').css('top', h2);
      $('.feat_1_2').css('opacity', 1);

      active = 1;

      $('.feats_1 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_1 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_2').on('click',function (e) {

    e.preventDefault();

    if($('.feat_1_1').css('opacity') == 1)
    return false;

    if($('.feat_2').css('opacity') == 1)
    {
      $('.feat_2').css('top', 0);
      $('.feat_2').css('opacity', 0);

      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';

      $('.feat_2').css('height', height);
      $('.feat_2').css('top', height);
      $('.feat_2').css('opacity', 1);

      active = 2;

      $('.feats_2 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_2 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_3').on('click',function (e) {

    e.preventDefault();

    if($('.feat_3').css('opacity') == 1)
    {
      $('.feat_3').css('left', 0);
      $('.feat_3').css('opacity', 0);
      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';

      var width = parseInt($(this).css('width'));
      width = width + 'px';

      $('.feat_3').css('height', height);
      $('.feat_3').css('left', width);
      $('.feat_3').css('opacity', 1);

      active = 3;

      $('.feats_3 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_3 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_4').on('click',function (e) {

    e.preventDefault();

    if($('.feat_3').css('opacity') == 1)
    return false;

    if($('.feat_4').css('opacity') == 1)
    {
      $('.feat_4').css('left', 0);
      $('.feat_4').css('opacity', 0);
      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';

      var width = parseInt($(this).css('width'));
      width = width + 'px';

      $('.feat_4').css('height', height);
      $('.feat_4').css('left', width);
      $('.feat_4').css('opacity', 1);

      active = 4;

      $('.feats_4 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_4 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_5').on('click',function (e) {

    e.preventDefault();

    if($('.feat_4').css('opacity') == 1)
    return false;

    if($('.feat_5').css('opacity') == 1)
    {
      $('.feat_5').css('top', 0);
      $('.feat_5').css('opacity', 0);
      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';


      $('.feat_5').css('height', height);
      $('.feat_5').css('top', height);
      $('.feat_5').css('opacity', 1);

      active = 5;

      $('.feats_5 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_5 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_6').on('click',function (e) {

    e.preventDefault();

    if($('.feat_1_1').css('opacity') == 1)
    return false;

    if($('.feat_6').css('opacity') == 1)
    {
      $('.feat_6').css('left', 0);
      $('.feat_6').css('opacity', 0);
      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';

      var width = parseInt($(this).css('width'));
      width = width + 'px';

      $('.feat_6').css('height', height);
      $('.feat_6').css('left', width);
      $('.feat_6').css('opacity', 1);

      active = 6;

      $('.feats_6 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_6 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_7').on('click',function (e) {

    e.preventDefault();

    if($('.feat_1_1').css('opacity') == 1 || $('.feat_2').css('opacity') == 1 || $('.feat_6').css('opacity') == 1)
    return false;

    if($('.feat_7').css('opacity') == 1)
    {
      $('.feat_7').css('left', 0);
      $('.feat_7').css('opacity', 0);
      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';

      var width = parseInt($(this).css('width'));
      width = width + 'px';

      $('.feat_7').css('height', height);
      $('.feat_7').css('left', width);
      $('.feat_7').css('opacity', 1);

      active = 7;

      $('.feats_7 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_7 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_8').on('click',function (e) {

    e.preventDefault();

    if($('.feat_7').css('opacity') == 1)
    return false;

    if($('.feat_8').css('opacity') == 1)
    {
      $('.feat_8').css('left', 0);
      $('.feat_8').css('opacity', 0);
      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';

      var width = parseInt($(this).css('width'));
      width = width + 'px';

      $('.feat_8').css('height', height);
      $('.feat_8').css('left', width);
      $('.feat_8').css('opacity', 1);

      active = 8;

      $('.feats_8 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_8 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_9').on('click',function (e) {

    e.preventDefault();

    if($('.feat_8').css('opacity') == 1  || $('.feat_10').css('opacity') == 1)
    return false;

    if($('.feat_9').css('opacity') == 1)
    {
      $('.feat_9').css('bottom', 0);
      $('.feat_9').css('opacity', 0);
      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';


      $('.feat_9').css('height', height);
      $('.feat_9').css('bottom', height);
      $('.feat_9').css('opacity', 1);

      active = 9;

      $('.feats_9 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_9 .custom_overlay)').show();
    } 

    });

  $('.feat_div .feats_10').on('click',function (e) {

    e.preventDefault();

    if($('.feat_5').css('opacity') == 1)
    return false;

    if($('.feat_10').css('opacity') == 1)
    {
      $('.feat_10').css('right', 0);
      $('.feat_10').css('opacity', 0);

      $('.custom_overlay').hide();
    } 

    else
    {
      
      hideFeat();

      var height = parseInt($(this).css('height'));
      height = height + 'px';

      var width = parseInt($(this).css('width'));
      width = width + 'px';

      $('.feat_10').css('height', height);
      $('.feat_10').css('right', width);
      $('.feat_10').css('opacity', 1);

      active = 10;

      $('.feats_10 .custom_overlay').hide();
      $('.feat_cols .custom_overlay:not(.feats_10 .custom_overlay)').show();
    } 

    });

});

window.onload = function() {

$('#loading').hide();

var windowWidth = $(window).width();
var windowHeight = $(window).height();

var headerHeight = parseInt($('.header_div').css('height'));
headerHeight = windowHeight - headerHeight;

var modelHeight = parseInt($('.model_1_header').css('height'));
var masterHeight = parseInt($('.master_plan_header').css('height'));

var headerHeight1 = windowHeight - modelHeight;
var headerHeight2 = windowHeight - masterHeight;

var feat_h = (headerHeight + 110) / 2;

feat_h = feat_h + 'px';
headerHeight = headerHeight + 'px';

$('.feat_cols').css('height',feat_h);

// $('.master_plan_img_div').css('min-height',headerHeight2);
// $('.direction_map_img_div').css('min-height',headerHeight2);

if(windowWidth > 767)
{
  newHeight = parseInt($('.header_div').css('height'));
  $('.fake_head').css('height', newHeight);

  // $('.about_right_col').css('min-height',headerHeight2);
  $('.about_left_col').css('height',parseInt($('.about_right_col').css('height')));
}

else
{
  newHeight = parseInt($('.mobile_nav').css('height'));
  $('.fake_head').css('height', newHeight);
}

}

$(window).on('resize', function()
{

  var windowWidth = $(window).width();

  if(windowWidth > 767)
  {
    var height = parseInt($('.about_right_col').outerHeight());
    height = height + 'px';

    $('.about_left_col').css('height', height);

    newHeight = parseInt($('.header_div').css('height'));
    $('.fake_head').css('height', newHeight);
  }

  else
  {
    $('.about_left_col').css('height', 'inherit');

    newHeight = parseInt($('.mobile_nav').css('height'));
    $('.fake_head').css('height', newHeight);
  }

  var head_height = parseInt($('.header_div').css('height'));
  $('.fake_head').css('height', head_height);


  /*********** Feature 1 *************/
  if($('.feat_div .feat_1_1').css('opacity') == 1)
  {
    var height = parseInt($('.feat_div .feats_1').css('height'));
    var width = parseInt($('.feat_div .feats_1').css('width'));
    width = width + 'px';
    
    var h1 = height * 2;
    var h2 = height;

    h1 = h1 + 'px';
    h2 = h2 + 'px';


    $('.feat_div .feat_1_1').css('height', h1);
    $('.feat_div .feat_1_1').css('left', width);

    $('.feat_div .feat_1_2').css('height', h2);
    $('.feat_div .feat_1_2').css('top', h2); 
  }
  /**********************************/
  /*********** Feature 2 *************/
  else if($('.feat_div .feat_2').css('opacity') == 1)
  {
    var height = parseInt($('.feat_div .feats_2').css('height'));

    height = height + 'px';


    $('.feat_2').css('height', height);
    $('.feat_2').css('top', height);
  }
  /**********************************/

  /*********** Feature 3 *************/
  else if($('.feat_div .feat_3').css('opacity') == 1)
  {
    var width = parseInt($('.feat_div .feats_3').css('width'));
    width = width + 'px';

    var height = parseInt($('.feat_div .feats_3').css('height'));
    height = height + 'px';

    $('.feat_3').css('left', width);
    $('.feat_3').css('height', height);
    $('.feat_3').css('top', 0);
  }
  /**********************************/

  /*********** Feature 4 *************/
  else if($('.feat_div .feat_4').css('opacity') == 1)
  {
    var width = parseInt($('.feat_div .feats_4').css('width'));
    width = width + 'px';

    var height = parseInt($('.feat_div .feats_4').css('height'));
    height = height + 'px';

    $('.feat_4').css('left', width);
    $('.feat_4').css('height', height);
    $('.feat_4').css('top', 0);
  }
  /**********************************/
  /*********** Feature 5 *************/
  else if($('.feat_div .feat_5').css('opacity') == 1)
  {
    var height = parseInt($('.feat_div .feats_5').css('height'));

    height = height + 'px';


    $('.feat_5').css('height', height);
    $('.feat_5').css('top', height);
  }
  /**********************************/
  /*********** Feature 6 *************/
  else if($('.feat_div .feat_6').css('opacity') == 1)
  {
    var width = parseInt($('.feat_div .feats_6').css('width'));
    width = width + 'px';

    var height = parseInt($('.feat_div .feats_6').css('height'));
    height = height + 'px';

    $('.feat_6').css('left', width);
    $('.feat_6').css('height', height);
    $('.feat_6').css('top', 0);
  }
  /**********************************/
  /*********** Feature 7 *************/
  else if($('.feat_div .feat_7').css('opacity') == 1)
  {
    var width = parseInt($('.feat_div .feats_7').css('width'));
    width = width + 'px';

    var height = parseInt($('.feat_div .feats_7').css('height'));
    height = height + 'px';

    $('.feat_7').css('left', width);
    $('.feat_7').css('height', height);
    $('.feat_7').css('top', 0);
  }
  /**********************************/
  /*********** Feature 8 *************/
  else if($('.feat_div .feat_8').css('opacity') == 1)
  {
    var width = parseInt($('.feat_div .feats_8').css('width'));
    width = width + 'px';

    var height = parseInt($('.feat_div .feats_8').css('height'));
    height = height + 'px';

    $('.feat_8').css('left', width);
    $('.feat_8').css('height', height);
    $('.feat_8').css('top', 0);
  }
  /**********************************/
  /*********** Feature 9 *************/
  else if($('.feat_div .feat_9').css('opacity') == 1)
  {
    var height = parseInt($('.feat_div .feats_9').css('height'));

    height = height + 'px';


    $('.feat_9').css('height', height);
    $('.feat_9').css('bottom', height);
  }
  /**********************************/
  /*********** Feature 10 *************/
  else if($('.feat_div .feat_10').css('opacity') == 1)
  {
    var width = parseInt($('.feat_div .feats_10').css('width'));
    width = width + 'px';

    var height = parseInt($('.feat_div .feats_10').css('height'));
    height = height + 'px';

    $('.feat_10').css('right', width);
    $('.feat_10').css('height', height);
    $('.feat_10').css('top', 0);
  }
  /**********************************/
  else
  {
    $('.feat_div .feat_1_1').css('left', 0);
    $('.feat_div .feat_1_2').css('top', 0); 
  }
  
  

});

$(window).scroll(function() {

  $('.down_btn').fadeOut();
    clearTimeout($.data(this, 'scrollTimer'));
    $.data(this, 'scrollTimer', setTimeout(function() {

        if($(window).scrollTop() + $(window).height() == $(document).height()) 
        {
          
        }
        else
        {
          $('.down_btn').fadeIn();
        }
        
    }, 400));
});

</script>