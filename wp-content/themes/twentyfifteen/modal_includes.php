<button class="md-trigger md_trigger_form" data-modal="modal-20"></button>

<div class="md-modal md-effect-9" id="modal-20">
  <div class="md-content">
        <h3>Download Brochure</h3>
        <div>
            <form class="form-horizontal pdf_submit" formID="2">
            <div class="form-group f_1">
              <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input inputID="1" name="name" type="text" class="form-control form_inputs form_name" required>
              </div>
            </div>
            <div class="form-group f_2">
              <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <input inputID="2" name="email" type="email" class="form-control form_inputs form_email" required>
              </div>
            </div>
            <div class="form-group f_2">
              <label for="inputPassword3" class="col-sm-2 control-label">Telephone Number</label>
              <div class="col-sm-10">
                <input inputID="3" name="phone" type="text" class="form-control form_inputs form_email" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <div class="send_spin">
                  <span><img src="assets/img/spinner.gif"/>Submitting</span>
                </div>
                <div class="succ_div">
                  <span>Submitted Successfully !</span>
                </div>
                <button type="submit" class="pdf_submit_btn form_btns btn btn-default">Submit</button>
              </div>
              <div class="download_pdf_btn">
                  <a href="<?= $brochure; ?>" download>
                  	<input type="button" class="btn btn-default" value="Download Brochure"/>
                  </a>
                </div>
            </div>
          </form>
        </div>
      </div>
  <button class="md-close">X</button>
</div>

<div class="md-overlay"></div>
<link rel="stylesheet" type="text/css" href="assets/modal/component.css" />
<script src="assets/modal/modernizr.custom.js"></script>
<script src="assets/modal/classie.js"></script>
<script src="assets/modal/modalEffects.js"></script>


